# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = ">= 2.4.1"
    }

    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.11.3"
    }

  }

  required_version = ">= 0.14.9"
}


# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

provider "kubectl" {
  # Configuration options
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kind-argocd-fdi"
}
