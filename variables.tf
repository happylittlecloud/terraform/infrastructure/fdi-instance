variable "minio_server_url" {
  description = "URL of the MinIO Server"
  default = "localhost:9000"
}

variable "gateway_name" {
  description = "Name of the MinIO Gateway"
}

variable "resource_group_name" {
  description = "Name of the Resource Group"
}

variable "storage_account_name" {
  description = "Name of the Storage Account"
}

variable "argocd_namespace" {
  description = "Namespace of the argocd instance"
}

variable "kubernetes_namespace" {
  description = "Namespace to deploy to"
}

variable "datasets" {
  description = "The map of datasets to create"
}
